#JQuery Plugins
2014-5-10   :jquery upload plugin<br/>
jquery异步上传插件<br/>
使用方式：看[demo](demo/upload.html)<br/>

2014-6-8	:leunpha.lazyload plugin<br/>
图片懒(延迟)加载<br/>
使用方式：看[demo](demo/lazy/test.htm)<br/>

2014-8-7    :js图片预览 plugin<br/>
js图片预览<br/>
使用方式：看[demo](demo/preview.htm)<br/>