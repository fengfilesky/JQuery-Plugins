var fs = require("fs"),
    url = require("url"),
    http = require("http"),
    path = require("path");

var leunpha = {
    template: {
        prev: '<a href="#href#">返回上一级</a><br/>',
        a: '<a href="#href#">#name#</a><br/> '
    },
    header: {
        html: function (response) {
            response.statusCode=200;
            response.setHeader('Content-Type','text/html;charset=utf-8');
        },
        text: function (response) {
            response.setHeader(200, {'Content-Type': 'text/plain'});
        },
        error: function(err,response){
            response.statusCode = 500;
            response.setHeader('Content-Type','text/html;charset=utf-8');
            response.write(err+"<br/>");
            response.end();
        },
        notFound: function(response){
            response.statusCode = 404;
            response.setHeader('Content-Type',"text/html;charset=utf-8");
            response.write("404 NOT FOUND!<br/>");
            response.end();
        }
    },
    file: {
        dir: function (filename, response) {
            fs.readdir(filename, function (err, files) {
                leunpha.header.html(response);
                filename = url.parse(filename).path;
                var lastIsLine =  (filename.charAt(filename.length-1) == "/");
                response.write(leunpha.template.prev.replace(/#href#/,lastIsLine?'../':'./'));
                files.forEach(function (d, i) {
                    var href = d;
                    if(!lastIsLine){
                        href = filename.substring(filename.lastIndexOf("/")+1,filename.length)+"/"+d;
                    }
                    response.write(leunpha.template.a.replace(/#name#/g, d).replace(/#href#/g, href));
                });
                response.end();
            });
        },
        read: function (filename, response) {
            path.exists(filename, function (exists) {
                if (!exists) {
                    leunpha.header.notFound(response);
                } else {
                    var stat = fs.lstatSync(filename);
                    if (stat.isDirectory()) {
                        leunpha.file.dir(filename, response);
                    } else {
                        leunpha.file.file(filename,response);
                    }
                }
            });
        },
        write: function(){

        },
        file: function (filename,response) {
          //  fs.readFile(filename,function (err,file) {
                var stream = fs.createReadStream(filename);
                stream.on("error",function(err){
                   leunpha.header.error(err,response);
                });
               /* stream.on("end",function(){
                    response.end();
                });*/
                stream.pipe(response);
          //  });
        }
    }
}


var server = http.createServer(function (request, response) {
    var uri = url.parse(request.url).pathname;
    var filename = path.join(__dirname, uri);

    leunpha.file.read(filename, response);


});
server.listen(81);
console.log("server is running , address : 127.0.0.1:81");