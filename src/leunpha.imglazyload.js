var leunpha = leunpha || {};
(function(){
	leunpha.imglazyload= function(options){
		options = options||{};
		var ops ={ preLoadHeight:options.preLoadHeight || 0,loadingImg: options.loadingImg||'loading.jpg'}
		onload = function(){
			var imgs = document.querySelectorAll("img");
			var b = function(){return document.body;},st=function(){return b().scrollTop;},cw=function(){return b().clientWidth;},ch=function(){return b().clientHeight;};
			for(var i=0;i<imgs.length;i++){
					var ds = imgs[i].getAttribute("data-src"),s=imgs[i].getAttribute("src");
					imgs[i].setAttribute("data-src",s);
					if(imgs[i].offsetTop > (st()+ch())){
						imgs[i].setAttribute("src",ops.loadingImg);
					}			
				}
			onscroll = function(){
				for(var i=0;i<imgs.length;i++){
					var ds = imgs[i].getAttribute("data-src"),s=imgs[i].getAttribute("src");
					if(imgs[i].offsetTop <= (st()+ch()+(ops.preLoadHeight||0)) && ds!=s){
						imgs[i].setAttribute("src",ds);
					}			
				}
			};
		}
	};
})();
